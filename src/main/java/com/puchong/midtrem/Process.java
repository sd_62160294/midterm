/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puchong.midtrem;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class Process {

    private static ArrayList<Item> itemList = new ArrayList<>();

    public static boolean addItem(Item item) {
        itemList.add(item);
        save();
        return true;
    }

    public static boolean delItem(Item item) {
        itemList.remove(item);
        save();
        return true;
    }

    public static boolean delItem(int index) {
        itemList.remove(index);
        save();
        return true;
    }

    public static ArrayList<Item> getItem() {
        return itemList;
    }

    public static boolean updataItem(int index, Item item) {
        itemList.set(index, item);
        save();
        return true;
    }

    public static Item getItem(int index) {
        return itemList.get(index);
    }

    public static boolean delAllItem() {
        itemList.removeAll(itemList);
        return true;
    }

    public static String total() {
        double total = 0;
        for (int i = 0; i < itemList.size(); i++) {
            total += itemList.get(i).getPrice() * itemList.get(i).getAmount();
        }
        save();
        return total + "";
    }

    public static String amount() {
        int amount = 0;
        for (int i = 0; i < itemList.size(); i++) {
            amount += itemList.get(i).getAmount();
        }
        save();
        return amount + "";
    }

    public static void save() {
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            file = new File("item.dat");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(itemList);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Process.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Process.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void load() {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            file = new File("item.dat");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            itemList = (ArrayList<Item>) ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Process.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Process.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Process.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
